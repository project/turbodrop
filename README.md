## CONTENTS OF THIS FILE ##

* Introduction
* Installation
* Configuration
* Usage
* Similar Projects
* How Can You Contribute?
* Maintainers

## INTRODUCTION ##

Author and maintainer: Chris Casutt (RealizationZone)
* Drupal: https://www.drupal.org/u/handkerchief
* Drupal: https://www.drupal.org/realizationzone
* Website: https://realization.zone/en (EN) | https://realization.zone/de (DE)
* Website: https://chriscasutt.ch/en (EN) | https://chriscasutt.ch/de (DE)

The module adds the HOTWIRE TURBO framework to your drupal project. The speed of a
single-page web application without having to write any JavaScript.
Install, activate and you're done.

1. Very simple: Integrates a local TURBO version into Drupal.
2. Increase and boost the speed of your web project.
3. Use this module to configure which Drupal areas TURBO may be activated for, e.g. frontend only, roles, etc.
4. Additional settings such as deleting the turbo cache are integrated.

## INSTALLATION ##

See https://www.drupal.org/docs/extending-drupal/installing-modules
for instructions on how to install or update Drupal modules.

## CONFIGURATION ##
Go to Configuration → Development → Turbodrop (/admin/config/development/turbodrop)
and activate the checkbox "Enable the Turbodrop functionality."

The default setting is that a local Javascript version of TURBO is used. However,
you can also use a different version by creating and specifying your own custom library. 
This specified custom library then also takes into account the settings of the module.

See https://www.drupal.org/node/2274843 for instructions on how to create a library.

If you want to use your own version, these links will help you:
* https://github.com/hotwired/turbo
* https://unpkg.com/browse/@hotwired/turbo@8.0.3/

### PERMISSIONS ###

The module permission 'Access and use of Turbodrop' and 'Administer Turbodrop' can
be configured under /admin/people/permissions or under /admin/people/permissions/module/turbodrop.

## USAGE ##

Here you will find instructions on how to use TURBO:
* https://turbo.hotwired.dev/handbook/introduction

1. Install and configure the module.
2. Use the TURBO syntax in your Drupal project.

## SIMILAR PROJECTS ##

* https://www.drupal.org/project/refreshless
  * The RefreshLess module is unfortunately somewhat frozen, but now uses similar technology.
    In contrast to RefreshLess, Turbodrop is a smaller and simpler module.

## HOW CAN YOU CONTRIBUTE? ##

* Report any bugs, feature or support requests in the issue tracker; if
  possible help out by submitting patches.
  https://www.drupal.org/project/issues/turbodrop
* If you would like to say thanks and support the development of this module, a
  donation will be much appreciated.
  https://www.paypal.com/donate/?hosted_button_id=NLQGNDY9GAND6
* Feel free to contact me for paid support: https://realization.zone/en/contact

## MAINTAINERS ##

Current maintainers:
* Chris Casutt (RealizationZone) - https://www.drupal.org/u/handkerchief