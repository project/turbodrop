<?php

namespace Drupal\turbodrop\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure turbodrop settings for this site.
 */
class TurbodropConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'turbodrop_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['turbodrop.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('turbodrop.settings');
    $config_enabled = $config->get('enabled');
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable the Turbodrop functionality.'),
      '#description' => $this->t('This setting has priority over other settings and permissions.'),
      '#default_value' => $config_enabled ?? TRUE,
    ];
    $form['integration'] = [
      '#type' => 'details',
      '#title' => $this->t('Library Integration'),
      '#open' => TRUE,
      '#description' => $this->t("Select the way the Turbo library is to be loaded."),
    ];
    $form['integration']['integration_type'] = [
      '#type' => 'radios',
      '#title' => $this->t('Types of integration'),
      '#options' => [
        'local' => 'Local - V-8.0.3',
        'custom' => 'Custom Library',
      ],
      '#required' => TRUE,
      '#default_value' => $config->get('integration_type') ?: 'local',
      '#description' => $this->t('If you want to use a CDN or another version, declare your own library and specify it here.'),
    ];
    $form['integration']['integration_library'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom Library'),
      '#default_value' => $config->get('integration_library') ?: '',
      '#size' => 60,
      '#maxlength' => 128,
      '#placeholder' => 'module_name/library_name',
      '#description' => $this->t('E.g. module_name/library_name. For defining your own drupal library, see <a href=":url">drupal.org</a>.', [':url' => 'https://www.drupal.org/node/2274843']),
      '#states' => [
        'required' => [
          ':input[name="integration_type"]' => ['value' => 'custom'],
        ],
        'visible' => [
          ':input[name="integration_type"]' => ['value' => 'custom'],
        ],
      ],
    ];
    $form['integration']['integration_area'] = [
      '#title' => t('Areas of integration'),
      '#type' => 'checkboxes',
      '#options' => [
        'theme_frontend' => 'Frontend theme',
        'theme_backend' => 'Backend/Administration theme',
        'route_admin' => 'Admin Routes',
      ],
      '#required' => TRUE,
      '#default_value' => array_filter($config->get('integration_area') ?: []) ?: ['theme_frontend'],
      '#description' => 'Choose in which area of Drupal Turbodrop should be activated. If you use the same theme for the frontend and backend, it can lead to unwanted effects, because the distinction in Drupal is not always clear.',
    ];

    $form['permissions'] = [
      '#type' => 'details',
      '#title' => $this->t('Permissions'),
      '#open' => TRUE,
      '#description' => $this->t("<strong>You have to set the permissions</strong></br>Normally, the function should be available for all roles. But if you want to exclude certain roles, you can set this accordingly."),
    ];
    $form['permissions']['access_role_administrator'] = [
      '#title' => t('Access and use of Turbodrop for role Administrator'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('access_role_administrator'),
      '#description' => "Because the Administrator role cannot be excluded in Drupal's normal permissions, it must be enabled here if desired.",
    ];
    $permission_destination = Url::fromRoute('turbodrop.configuration')->toString();
    $form['permissions']['role_access'] = [
      '#type' => 'link',
      '#title' => $this->t('Permissions for module Turbodrop'),
      '#url' => Url::fromRoute('user.admin_permissions.module', ['modules' => 'turbodrop'], ['query' => ['destination' => $permission_destination]]),
    ];

    $form['turbo_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Turbo Configuration'),
      '#open' => TRUE,
    ];
    $form['turbo_config']['drive_cache_clear'] = [
      '#title' => 'Turbo.cache.clear()',
      '#type' => 'checkbox',
      '#default_value' => $config->get('drive_cache_clear'),
      '#description' => "Turbo.cache.clear()",
    ];
    $form['turbo_config']['drive_session'] = [
      '#title' => 'Turbo.session.drive',
      '#type' => 'checkbox',
      '#default_value' => $config->get('drive_session'),
      '#description' => "Turbo.session.drive = true/false",
    ];
    $form['turbo_config']['drive_progressbar_delay'] = [
      '#title' => 'Turbo.setProgressBarDelay()',
      '#type' => 'number',
      '#step' => 1,
      '#min' => 0,
      '#placeholder' => '500',
      '#default_value' => $config->get('drive_progressbar_delay'),
      '#description' => "Turbo.setProgressBarDelay(delayInMilliseconds)",
    ];
    $form['#attached']['library'][] = 'turbodrop/turbodrop-configuration';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('integration_type') === 'custom') {
      $custom_library_name = $form_state->getValue('integration_library');
      if (empty($custom_library_name)) {
        $form_state->setErrorByName('integration_library', 'The library field is empty.');
      }
      else {
        /** @var \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery */
        $library_discovery = \Drupal::service('library.discovery');
        [$extension, $name] = explode('/', $custom_library_name, 2);
        $definition = $library_discovery->getLibraryByName($extension, $name);
        if (!$definition) {
          $form_state->setErrorByName('integration_library', "This library doesn't exist.");
        }
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('turbodrop.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('integration_type', $form_state->getValue('integration_type'))
      ->set('integration_library', $form_state->getValue('integration_library'))
      ->set('integration_area', $form_state->getValue('integration_area'))
      ->set('access_role_administrator', $form_state->getValue('access_role_administrator'))
      ->set('drive_cache_clear', $form_state->getValue('drive_cache_clear'))
      ->set('drive_session', $form_state->getValue('drive_session'))
      ->set('drive_progressbar_delay', $form_state->getValue('drive_progressbar_delay'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
