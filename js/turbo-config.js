/**
 * @file
 * Attaches behaviors for Turbo configurations.
 */

(function (Drupal, drupalSettings) {
  Drupal.behaviors.turbodropConfig = {
    attach: function (context, settings) {
      once('turbodropConfig', 'html').forEach(function (element) {

        const tdSettings = drupalSettings.turbodropSettings;
        console.log(drupalSettings.turbodropSettings);
        if (typeof Turbo !== 'undefined') {

          if (tdSettings.enabled == 1) {
            if (tdSettings.drive_cache_clear == 1) {
              Turbo.cache.clear()
            }
            if (tdSettings.drive_session == 1) {
              Turbo.session.drive = true;
            }
            else {
              Turbo.session.drive = false;
            }
            if (tdSettings.drive_progressbar_delay != "") {
              Turbo.setProgressBarDelay(tdSettings.drive_progressbar_delay);
            }
          }
          else {
            Turbo.session.drive = false;
          }
        }
      })
    }
  }
})(Drupal, drupalSettings);
